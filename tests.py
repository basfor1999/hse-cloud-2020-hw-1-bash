import os
import pytest
from subprocess import Popen, PIPE


INPUT_OUTPUT_TASKS_PATHS = [
    'tasks/bash_tutorials_getting_started_with_conditionals',
    'tasks/text_processing_cut_5',
    'tasks/text_processing_in_linux_the_uniq_command_4',
    'tasks/paste_4',
    'tasks/bash_tutorials_more_on_conditionals',
    'tasks/text_processing_in_linux_the_uniq_command_2',
    'tasks/bash_tutorials_a_personalized_echo',
    'tasks/text_processing_cut_3',
    'tasks/text_processing_tr_1',
    'tasks/bash_tutorials_slice_an_array',
    'tasks/bash_tutorials_looping_with_numbers',
    'tasks/awk_4',
    'tasks/awk_3',
    'tasks/bash_tutorials_filter_an_array_with_patterns',
    'tasks/text_processing_sort_5',
    'tasks/text_processing_in_linux_the_sed_command_3',
    'tasks/bash_tutorials_remove_the_first_capital_letter_from_each_array_element',
    'tasks/text_processing_tr_2',
    'tasks/bash_tutorials_comparing_numbers',
    'tasks/bash_tutorials_count_the_number_of_elements_in_an_array',
    'tasks/sed_command_5',
    'tasks/bash_tutorials_compute_the_average',
    'tasks/text_processing_tr_3',
    'tasks/text_processing_head_2',
    'tasks/sed_command_4',
    'tasks/text_processing_in_linux_the_middle_of_a_text_file',
    'tasks/bash_tutorials_arithmetic_operations',
    'tasks/awk_1',
    'tasks/bash_tutorials_concatenate_an_array_with_itself'
]


class TestInputOutputTasks:
    @pytest.mark.parametrize('task_path', INPUT_OUTPUT_TASKS_PATHS)
    def test_check_output(self, task_path):
        run_sh = os.path.join(task_path, 'run.sh')
        assert os.path.exists(run_sh), 'Solution file \'run.sh\' not found'
        input_path = os.path.join(task_path, 'input')
        output_path = os.path.join(task_path, 'output')
        tests = os.listdir(input_path)
        for t in tests:
            with open(os.path.join(input_path, t), 'rb') as stdin_io:
                p = Popen([run_sh], stdin=PIPE, stdout=PIPE, stderr=PIPE)
                p.stdin.write(stdin_io.read())
                p.stdin.close()
                p.wait()
                assert p.returncode == 0, 'Non-zero return code: ' + str(p.returncode)
                assert p.stderr.read() == b''
            with open(os.path.join(output_path, t), 'rb') as stdout_io:
                result = p.stdout.read()
                stdout = stdout_io.read()
                assert (len(result) > 1 and result[:-1] == stdout) or result == stdout