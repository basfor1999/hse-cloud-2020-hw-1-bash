#!/bin/bash
N=0
echo -n "$(while IFS= read -r line
do
  a+=($line)
  N=$((N + 1))
done

for ((i=0; i < N; i++)) do
  if [[ $i == 0 ]]; then
    if [[ ${a[i]} != ${a[i + 1]} ]]; then
      echo ${a[i]}
    fi
  elif [[ $i == $((N - 1)) ]]; then
    if [[ ${a[i]} != ${a[i - 1]} ]]; then
      echo -n ${a[i]}
    fi
  elif [[ ${a[i]} != ${a[i + 1]} &&  ${a[i]} != ${a[i - 1]} ]]; then
    echo ${a[i]}
  fi
done
)"