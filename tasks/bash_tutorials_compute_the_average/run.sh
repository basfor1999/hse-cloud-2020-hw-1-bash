#!/bin/bash
sum=0
count=0
read -r N
for ((i=0; i < N; i++)) do
  read -r line
  sum=$((sum + line))
  count=$((count + 1))
done
echo $(env printf %.3f $(echo "scale=4; $sum / $count" | bc)) | tr ',' '.'