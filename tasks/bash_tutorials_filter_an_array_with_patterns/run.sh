#!/bin/bash
while IFS= read -r line
do
  a+=($line)
done

for c in "${a[@]}"; do
  if [[ $(echo $c | grep "[aA]") == "" ]]; then
    echo $c
  fi
done