#!/bin/bash
s=""
i=0
while IFS= read -r line
do
  if [[ 3 -le $i && $i -le 7 ]]; then
    s="$s$line "
  fi
  i=$((i + 1))
done
echo "${s::-1}"