#!/bin/bash
s=""
while IFS= read -r line
do
  s="$s.${line:1} "
done
echo "${s::-1}"