#!/bin/bash
read a
read b
read c
if [[ $c == $a && $a == $b ]]; then
  echo -n "EQUILATERAL"
elif [[ $c == $a || $a == $b || $b == $c ]]; then
  echo -n "ISOSCELES"
else
  echo -n "SCALENE"
fi